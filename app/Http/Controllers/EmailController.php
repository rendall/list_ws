<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Carousel;
use App\Mail\Contacto;
use App\Mail\RegistroSebasEmail;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function contactar(Request $request){
        
        
        $objDemo = new \stdClass();
        $objDemo->nombre = $request->nombre;
        $objDemo->email = $request->email;
        $objDemo->mensaje = $request->mensaje;
        $objDemo->fecha=date('Y-m-d');
        $objDemo->ip=$request->ip();
        $limite=Carousel::listar_correos($objDemo->fecha,$objDemo->ip);
        if($limite<6){
            $estado = Carousel::contacta_bd($objDemo);
            //return $estado;
            if($estado>0)
            {
                $email_admin='info@listaso.com';
                //$email_admin='r18rendall@gmail.com';
                Mail::to($email_admin)->send(new Contacto($objDemo));
                return response()->json($estado);
            }else{
                return 'Ocurrio un error al enviar email';
            }
        }else{
            return 'Ya ha consumido los intentos validos para enviar email';
        }
        
    }
}
