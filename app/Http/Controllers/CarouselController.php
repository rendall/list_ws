<?php

namespace App\Http\Controllers;

use App\Models\Carousel;
use Illuminate\Http\Request;

class CarouselController extends Controller
{
    public function lists($id_modulo,$id_idioma){
        return response()->json(Carousel::listar($id_modulo,$id_idioma));
    }

    public function lists_mobile($id_modulo,$id_idioma){
        return response()->json(Carousel::listar_mobile_platform($id_modulo,$id_idioma));
    }

    public function lists_color_circle($id_modulo,$id_idioma){
        return response()->json(Carousel::listar_color_circle($id_modulo,$id_idioma));
    }

    public function lists_banner($id_modulo,$id_idioma){
        return response()->json(Carousel::listar_banner($id_modulo,$id_idioma));
    }

    public function lists_about_listaso_values($id_modulo,$id_idioma){
        return response()->json(Carousel::listar_about_listaso_values($id_modulo,$id_idioma));
    }

    public function lists_info_contact($id_modulo,$id_idioma){
        return response()->json(Carousel::listar_info_contacto($id_modulo,$id_idioma));
    }

    public function lists_sales($id_modulo,$id_idioma){
        return response()->json(Carousel::listar_aplicacion_de_ventas($id_modulo,$id_idioma));
    }
    
    public function lists_cuadro_colores($id_modulo,$id_idioma){
        return response()->json(Carousel::listar_cuadros_colores($id_modulo,$id_idioma));
    }

    public function lists_check_orange($id_modulo,$id_idioma,$color){
        return response()->json(Carousel::listar_check_naranja($id_modulo,$id_idioma,$color));
    }

    public function lists_customerlite($id_modulo,$id_idioma){
        return response()->json(Carousel::listar_customerlite($id_modulo,$id_idioma));
    }

    public function lists_boxcolor($id_modulo,$id_idioma){
        return response()->json(Carousel::listar_boxcolor($id_modulo,$id_idioma));
    }

    public function lists_integrations($id_modulo,$id_idioma){
        return response()->json(Carousel::listar_integraciones($id_modulo,$id_idioma));
    }
    public function lists_multitexto($id_modulo,$id_idioma){
        return response()->json(Carousel::listar_multitexto($id_modulo,$id_idioma));
    }
    public function list_info_int($id_modulo,$id_idioma){
        return response()->json(Carousel::listar_info_int($id_modulo,$id_idioma));
    }

    public function list_pricing($id_modulo,$id_idioma){
        return response()->json(Carousel::listar_precios($id_modulo,$id_idioma));
    }
}
