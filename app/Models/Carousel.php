<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Carousel extends Model
{
    public static function listar($id_modulo,$id_idioma){
        $aw=array();

        $data= DB::table('carousel')
        ->select('imagen1', 'imagen2','imagen3','imagen4','titulo1','titulo2')
        ->where([
                    ['estado','=',1],
                    ['idmodulo','=',$id_modulo],
                    ['ididioma','=',$id_idioma]
                ])
        ->get();

        $aw['carousels']=$data;                                
        return $aw;
            //if(count($data)>0)
    }
    public static function listar_mobile_platform($id_modulo,$id_idioma){
        $aw=array();

        $data= DB::table('mobileplatform')
        ->select('imagen', 'titulo','descripcion')
        ->where([
                    ['estado','=',1],
                    ['idmodulo','=',$id_modulo],
                    ['ididioma','=',$id_idioma]
                ])
        ->get();

        $aw['mobileplatform']=$data;                                
        return $aw;
    }
    public static function listar_color_circle($id_modulo,$id_idioma){
        $aw=array();

        $data= DB::table('colorscircle')
        ->select( 'titulo','descripcion')
        ->where([
                    ['estado','=',1],
                    ['idmodulo','=',$id_modulo],
                    ['ididioma','=',$id_idioma]
                ])
        ->get();

        $aw['colorscircle']=$data;                                
        return $aw;
    }

    public static function listar_banner($id_modulo,$id_idioma){
        $aw=array();

        $data= DB::table('banner')
        ->select( 'titulo1','titulo2','imagen')
        ->where([
                    ['estado','=',1],
                    ['idmodulo','=',$id_modulo],
                    ['ididioma','=',$id_idioma]
                ])
        ->get();

        $aw['banner']=$data;                                
        return $aw;
    }

    public static function listar_about_listaso_values($id_modulo,$id_idioma){
        $aw=array();

        $data= DB::table('aboutandbackoffice')
        ->select( 'titulo','descripcion','imagen')
        ->where([
                    ['estado','=',1],
                    ['idmodulo','=',$id_modulo],
                    ['ididioma','=',$id_idioma]
                ])
        ->get();

        $aw['about_listaso_values']=$data;                                
        return $aw;
    }

    public static function listar_info_contacto($id_modulo,$id_idioma){
        $aw=array();

        $data= DB::table('contacto')
        ->select( 'direccion1','direccion2','direccion3','email','item1','telefono')
        ->where([
                    ['estado','=',1],
                    ['idmodulo','=',$id_modulo],
                    ['ididioma','=',$id_idioma]
                ])
        ->get();

        $aw['info_contacto']=$data;                                
        return $aw;
    }

    public static function listar_aplicacion_de_ventas($id_modulo,$id_idioma){
        $aw=array();

        $data= DB::table('paralax')
        ->select( 'titulo1','descripcion1','descripcion2','imagen')
        ->where([
                    ['estado','=',1],
                    ['idmodulo','=',$id_modulo],
                    ['ididioma','=',$id_idioma]
                ])
        ->get();

        $aw['sales_paralax']=$data;                                
        return $aw;
    }

    public static function listar_cuadros_colores($id_modulo,$id_idioma){
        $aw=array();

        $data= DB::table('cuadrocolores')
        ->select( 'bigTitulo','smallTitulo','descripcion')
        ->where([
                    ['estado','=',1],
                    ['idmodulo','=',$id_modulo],
                    ['ididioma','=',$id_idioma]
                ])
        ->get();

        $aw['cuadros_colores']=$data;                                
        return $aw;
    }

    public static function listar_check_naranja($id_modulo,$id_idioma,$color){
        $aw=array();

        $data= DB::table('orangeckeck')
        ->select( 'titulo','descripcion','ch')
        ->where([
                    ['ch','=',$color],
                    ['estado','=',1],
                    ['idmodulo','=',$id_modulo],
                    ['ididioma','=',$id_idioma]
                ])
        ->get();

        $aw['check_orange']=$data;                                
        return $aw;
    }

    public static function listar_customerlite($id_modulo,$id_idioma){
        $aw=array();

        $data= DB::table('customerlite')
        ->select( 'descripcion','imagen','item1','item2','item3','item4','item5','item6')
        ->where([
                    ['estado','=',1],
                    ['idmodulo','=',$id_modulo],
                    ['ididioma','=',$id_idioma]
                ])
        ->get();

        $aw['customerlite']=$data;                                
        return $aw;
    }

    public static function listar_boxcolor($id_modulo,$id_idioma){
        $aw=array();

        $data= DB::table('boxcolor')
        ->select( 'titulo','descripcion','imagen','item1','item2','item3','item4','item5','item6','item7','imagen','color')
        ->where([
                    ['estado','=',1],
                    ['idmodulo','=',$id_modulo],
                    ['ididioma','=',$id_idioma]
                ])
        ->get();
        if($data[0]->color==0){
            $aw['boxcolor']['rojo']=$data[0];
            $aw['boxcolor']['verde']=$data[1];
        }else{
            $aw['boxcolor']['rojo']=$data[1];
            $aw['boxcolor']['verde']=$data[0];
        }                              
        return $aw;
    }

    public static function listar_integraciones($id_modulo,$id_idioma){
        $aw=array();

        $data= DB::table('integracion')
        ->select( 'titulo1','titulo2','titulo3','descripcion1','descripcion2','descripcion3','descripcion4')
        ->where([
                    ['estado','=',1],
                    ['idmodulo','=',$id_modulo],
                    ['ididioma','=',$id_idioma]
                ])
        ->get();

        $aw['integracion']=$data;                                
        return $aw;
    }

    public static function listar_multitexto($id_modulo,$id_idioma){
        $aw=array();

        $data= DB::table('multitexto')
        ->select( 'titulo','descripcion1','descripcion2','descripcion3','descripcion4')
        ->where([
                    ['estado','=',1],
                    ['idmodulo','=',$id_modulo],
                    ['ididioma','=',$id_idioma]
                ])
        ->get();

        $aw['multitexto']=$data;                                
        return $aw;
    }

    public static function listar_info_int($id_modulo,$id_idioma){
        $aw=array();

        $data= DB::table('informacionintegracion')
        ->select( 'titulo1','titulo2','descripcion1','descripcion2','descripcion3','descripcion4')
        ->where([
                    ['estado','=',1],
                    ['idmodulo','=',$id_modulo],
                    ['ididioma','=',$id_idioma]
                ])
        ->get();

        $aw['info_int']=$data;                                
        return $aw;
    }

    public static function listar_precios($id_modulo,$id_idioma){
        $aw=array();

        $data= DB::table('precio')
        ->select( 'titulo1','titulo2','precio','item1','item2','item3','item4','item5','item6','item7','item8','item9','item10','item11','item12')
        ->where([
                    ['estado','=',1],
                    ['idmodulo','=',$id_modulo],
                    ['ididioma','=',$id_idioma]
                ])
        ->get();

        $aw['precio']=$data;                                
        return $aw;
    }
    public static function contacta_bd($objeto)
   {
        $res=DB::table('email_usuario')->insertGetId([
            'nombre' => $objeto->nombre,
            'email' => $objeto->email,
            'mensaje' => $objeto->mensaje,
            'fecha' => $objeto->fecha,
            'ip' => $objeto->ip
        ]);
        return $res;
   }
   public static function listar_correos($fecha,$ip){
    $aw=array();

    $data= DB::table('email_usuario')
    ->where([
                ['fecha','=',$fecha],
                ['ip','=',$ip],
            ])
    ->get();

    $numero=count($data);                                
    return $numero;
}
    
}
