<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//----------------INDEX-----------------------------------------------modulo1
//arreglar rutas img
Route::get('/carousel/{id_modulo}/{id_idioma}','CarouselController@lists');
//arreglar rutas img
Route::get('/mobile_platform/{id_modulo}/{id_idioma}','CarouselController@lists_mobile');
Route::get('/colors_clicle/{id_modulo}/{id_idioma}','CarouselController@lists_color_circle');

//----------------ABAUT-----------------------------------------------modulo2
//arreglar rutas img (esta ruta sirve para about us y contact us)
Route::get('/banner/{id_modulo}/{id_idioma}','CarouselController@lists_banner');
//arreglar rutas img Listaso values
Route::get('/listaso_values/{id_modulo}/{id_idioma}','CarouselController@lists_about_listaso_values');

//--------------Contact-----------------------------------------------modulo3
Route::get('/info_contact/{id_modulo}/{id_idioma}','CarouselController@lists_info_contact');

//------------Mobile Apps (Listaso Sales App)-------------------------modulo4
//arreglar rutas img
Route::get('/paralax/{id_modulo}/{id_idioma}','CarouselController@lists_sales');
Route::get('/cuadro_colores/{id_modulo}/{id_idioma}','CarouselController@lists_cuadro_colores');

//------------Mobile Apps (Listaso Inventory App)---------------------modulo5

//------------Mobile Apps (Listaso backoffice)------------------------modulo6
Route::get('/check_orange/{id_modulo}/{id_idioma}/{color}','CarouselController@lists_check_orange');

//------------Mobile Apps (Listaso Lite)------------------------modulo7
Route::get('/customerlite/{id_modulo}/{id_idioma}','CarouselController@lists_customerlite');
Route::get('/boxcolor/{id_modulo}/{id_idioma}','CarouselController@lists_boxcolor');

//------------Integrations (QuickBooks)-------------------------modulo8
Route::get('/integrations/{id_modulo}/{id_idioma}','CarouselController@lists_integrations');
Route::get('/multitexto/{id_modulo}/{id_idioma}','CarouselController@lists_multitexto');
//------------------modulo9 (llamo a servcios existentes)
//---------------Integrations (SAP) ----------------------------modulo10
Route::get('/info_int/{id_modulo}/{id_idioma}','CarouselController@list_info_int');
//------------------modulo 11 y 12 (llamo a servcios existentes)
//---------------Precios  -------------------------------------modulo13
Route::get('/precios/{id_modulo}/{id_idioma}','CarouselController@list_pricing');

Route::post('/contact', 'EmailController@contactar');
/* configuarar archivo .env y archivo config/mail  */
/* activar acceso de aplicaciones menos segura en la cuenta de gmail */
